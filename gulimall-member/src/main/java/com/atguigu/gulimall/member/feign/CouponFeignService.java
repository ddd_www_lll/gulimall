package com.atguigu.gulimall.member.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author dwl
 * @Date 2024-02-13
 */
@FeignClient("gulimall-coupon")
public interface CouponFeignService {

    /**
     * 这是一个声明式的远程调用
     * @return
     */

    @RequestMapping("/coupon/coupon/member/list")
    public R membercoupons();
}
