package com.atguigu.gulimall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 如何使用nacos作为配置中心统一管理配置：
 * 1、引入nacos配置中心依赖
 * 2、创建一个bootstrap.properties
 *      spring:
 *   application:
 *     name: gulimall-coupon
 *   cloud:
 *     nacos:
 *       config:
 *         server-addr: 127.0.0.1:8848
 *  3、给配置中心默认添加一个叫数据集（dataid）默认规则：应用名.properties
 *  4、应用名.properties 添加任何配置
 *  5、动态获取配置
 * @RefreshScopee：动态获取并刷新配置
 * @Value("${配置项名}")：获取到配置
 *  如果配置中心和当前配置文件中都配置了相同的项，优先使用配置中心的配置
 *
 *  命名空间：配置隔离；
 *      默认：public（保留空间）；默认新增的所有配置都在public下
 *      1、开发、测试、生产：利用命名空间做环境隔离
 *      注意：在bootstrap.properties配置中，需要使用哪个命名空间下的配置
 *       cloud:
 *     nacos:
 *       config:
 *         namespace: 44b5949e-8277-4cac-a9da-4f6f0b0fa381
 *      2、每一个微服务之间相互隔离配置，每一个微服务都创建自己的命名空间，只加载自己的命名空间的所有配置
 *  配置集：所有配置的集合
 *  配置集ID：类似文件名。
 *      dataID：类似文件名
 *  配置分组：默认所有的配置集都属于：default_group
 *
 *  每个微服务创建自己的命名空间，配置分组区分环境
 *
 *  3、同时加载多个配置集：
 *      微服务任何配置信息，任何配置文件都可以放在配置中心中
 *      只需要在bootstrap.properties中说明加载配置中心的哪些配置文件即可
 *      @Value，@ConfigProperties以前springboot任何方法都可以从配置文件中获取值都可以使用
 *
 */

@SpringBootApplication
@EnableDiscoveryClient
public class GulimallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallCouponApplication.class, args);
    }

}
