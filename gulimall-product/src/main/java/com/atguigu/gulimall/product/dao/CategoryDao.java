package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author dwl
 * @email dwl@gulimall.com
 * @date 2024-02-13 13:10:21
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
