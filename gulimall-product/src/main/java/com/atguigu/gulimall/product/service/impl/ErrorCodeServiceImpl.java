package com.atguigu.gulimall.product.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.product.dao.ErrorCodeDao;
import com.atguigu.gulimall.product.entity.ErrorCodeEntity;
import com.atguigu.gulimall.product.service.ErrorCodeService;


@Service("errorCodeService")
public class ErrorCodeServiceImpl extends ServiceImpl<ErrorCodeDao, ErrorCodeEntity> implements ErrorCodeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ErrorCodeEntity> page = this.page(
                new Query<ErrorCodeEntity>().getPage(params),
                new QueryWrapper<ErrorCodeEntity>()
        );

        return new PageUtils(page);
    }

}