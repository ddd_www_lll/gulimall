package com.atguigu.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.ErrorCodeEntity;

import java.util.Map;

/**
 * 
 *
 * @author dwl
 * @email dwl@gulimall.com
 * @date 2024-02-13 13:10:21
 */
public interface ErrorCodeService extends IService<ErrorCodeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

