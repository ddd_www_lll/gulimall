package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * ??Ʒ?
 * 
 * @author dwl
 * @email dwl@gulimall.com
 * @date 2024-02-13 13:10:20
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
