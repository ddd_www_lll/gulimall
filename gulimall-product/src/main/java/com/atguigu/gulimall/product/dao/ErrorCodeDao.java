package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.ErrorCodeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author dwl
 * @email dwl@gulimall.com
 * @date 2024-02-13 13:10:21
 */
@Mapper
public interface ErrorCodeDao extends BaseMapper<ErrorCodeEntity> {
	
}
