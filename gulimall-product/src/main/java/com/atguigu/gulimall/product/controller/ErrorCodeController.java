package com.atguigu.gulimall.product.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.atguigu.gulimall.product.entity.ErrorCodeEntity;
import com.atguigu.gulimall.product.service.ErrorCodeService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;



/**
 * 
 *
 * @author dwl
 * @email dwl@gulimall.com
 * @date 2024-02-13 13:10:21
 */
@RestController
@RequestMapping("gulimallproduct/errorcode")
public class ErrorCodeController {
    @Autowired
    private ErrorCodeService errorCodeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("gulimallproduct:errorcode:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = errorCodeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("gulimallproduct:errorcode:info")
    public R info(@PathVariable("id") Integer id){
		ErrorCodeEntity errorCode = errorCodeService.getById(id);

        return R.ok().put("errorCode", errorCode);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("gulimallproduct:errorcode:save")
    public R save(@RequestBody ErrorCodeEntity errorCode){
		errorCodeService.save(errorCode);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("gulimallproduct:errorcode:update")
    public R update(@RequestBody ErrorCodeEntity errorCode){
		errorCodeService.updateById(errorCode);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("gulimallproduct:errorcode:delete")
    public R delete(@RequestBody Integer[] ids){
		errorCodeService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
